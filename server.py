"""myprettyappname - myprettyappname

Usage:
    myprettyappname.py server run [--port=<port_num>] [--debug] [--local]
    myprettyappname.py server routes
    myprettyappname.py --help

Options:
    -p --port=<port_num>   Set server port [default: 5000]
    -D --debug             Run in debug mode
    -l --local             Run the server locally
    -h --help              Show this screen.
"""

from docopt import docopt
from frankenstein.server import application

arguments = docopt(__doc__)
if arguments['server']:
    if arguments['run']:
        application.run(port=int(arguments['--port']), debug=arguments['--debug'], local=arguments['--local'])

    if arguments['routes']:
        application.show_routes()
