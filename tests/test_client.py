from frankenstein.client import Project

import unittest

class TestClient(unittest.TestCase):
    def test_app_valid_names(self, *args, **kwargs):
        names = ['app', 'appname', 'asd', 'thisoneisratherlong']

        for name in names:
            try:
                Project(name, ['hi'])
            except ValueError, e:
                self.fail('Name %s should be valid for app' % name)

    def test_app_invalid_names(self, *args, **kwargs):
        names = ['1app', 'app1', 'ap1p', '123', '123r', 'ab', 'a', 'App', 'aPp',
            'apP', 'app p', 'app-', '-app', 'ap-p', '_app', 'ap_p', 'ap!p',]

        for name in names:
            try:
                Project(name, ['hi'])
                self.fail('Name %s should be invalid for app' % name)
            except ValueError, e:
                assert 'app name is invalid' in str(e).lower(), \
                'Wrong exception raised:\n  %s' % str(e)
