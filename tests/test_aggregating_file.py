import json
import unittest

from frankenstein.aggregating_file import AggregatingFile


class TestAggregatingFile(unittest.TestCase):
    # @mock.patch('troll_client.troll_client.request_ips', side_effect=getRequestList.json)
    def test_block_valid_names(self, *args, **kwargs):
        sources = [
            '#& BLOCK-BEGIN one &##& BLOCK-END one &#',
            'header#& BLOCK-BEGIN one &##& BLOCK-END one &#',
            '#&BLOCK-BEGIN one&##& BLOCK-END one &#footer',
            '#& BLOCK-BEGIN one &#content#& BLOCK-END one &#footer',
            '#&BLOCK-BEGIN a &##&BLOCK-END a&#',
            '#& BLOCK-BEGIN CamelCase &##& BLOCK-END CamelCase &#',
            '#& BLOCK-BEGIN with-this &##& BLOCK-END with-this &#',
            '#& BLOCK-BEGIN and_this_ &##& BLOCK-END and_this_ &#',
            '#&BLOCK-BEGIN a &##& BLOCK-END a &##& BLOCK-BEGIN b &##& BLOCK-END b &#',
            '#& BLOCK-BEGIN b &##& BLOCK-END b &##& BLOCK-BEGIN a &##& BLOCK-END a &#',
            """#& BLOCK-BEGIN a &##& BLOCK-END a &##& BLOCK-BEGIN b &#.............a
            #& BLOCK-END b &#asd4444444#& BLOCK-BEGIN c &#aaasasas#& BLOCK-END c &##& BLOCK-BEGIN d &##& BLOCK-END d &#
            1111""",
            'joooooooooooo',
        ]

        names = [
            ['one'], ['one'], ['one'], ['one'], ['a'], ['CamelCase'], ['with-this'], ['and_this_'], ['a', 'b'],
            ['b', 'a'], ['a', 'b', 'c', 'd'], []
        ]

        for idx in range(len(sources)):
            agr_file = AggregatingFile(sources[idx])
            block_names = [b['name'] for b in agr_file.blocks]
            assert block_names == names[idx], 'Source:\n%s\nShould be %s but got %s' % (
                sources[idx], names[idx], block_names)

    def test_block_invalid_names(self, *args, **kwargs):
        sources = [
            '#& BLOCK-BEGIN not valid &##& BLOCK-END not valid &#',
            '#& BLOCK-BEGIN #la &##& BLOCK-END #la &#',
            '#&BLOCK-BEGINa&##& BLOCK-END a &#',
            '#& block-begin #la &##& BLOCK-END #la &#',
        ]

        names = [
            [], [], [], [],
        ]

        for idx in range(len(sources)):
            agr_file = AggregatingFile(sources[idx])
            block_names = [b['name'] for b in agr_file.blocks]
            assert block_names == names[idx], 'Source:\n%s\nShould be %s but got %s' % (
                sources[idx], names[idx], block_names)

    def test_block_unclosed(self, *args, **kwargs):
        sources = [
            '#& BLOCK-BEGIN a &#',
            '#& BLOCK-BEGIN one &##& BLOCK-END one &##& BLOCK-BEGIN a &#',
            '#& BLOCK-BEGIN a &##& BLOCK-BEGIN b &##& BLOCK-END b &#',
        ]

        for idx in range(len(sources)):
            try:
                agr_file = AggregatingFile(sources[idx])
                assert False, 'Source:\n%s\nShould raise' % sources[idx]
            except Exception as exc:
                assert 'was not closed' in exc.message.lower(), 'Source:\n%s\nDid not raise the right exception: %s' \
                                                                % (sources[idx], exc.message)

    def test_block_multiple_names(self, *args, **kwargs):
        sources = [
            '#& BLOCK-BEGIN a &##& BLOCK-END a &##& BLOCK-BEGIN a &##& BLOCK-END a &#',
            '#& BLOCK-BEGIN a &##& BLOCK-END a &##& BLOCK-BEGIN b &##& BLOCK-END b &##& BLOCK-BEGIN a &##& BLOCK-END a &#',
        ]

        for idx in range(len(sources)):
            try:
                agr_file = AggregatingFile(sources[idx])
                assert False, 'Source:\n%s\nShould raise' % sources[idx]
            except Exception as exc:
                assert 'already defined' in exc.message.lower(), 'Source:\n%s\nDid not raise the right exception: %s' \
                                                                 % (sources[idx], exc.message)

    def test_block_before(self, *args, **kwargs):
        sources = [
            'this comes before#& BLOCK-BEGIN one &##& BLOCK-END one &#',
            '#& BLOCK-BEGIN one &##& BLOCK-END one &#',
            ' #& BLOCK-BEGIN one &##& BLOCK-END one &#',
            'not a thing here',
            '#&BLOCK-BEGIN a &##& BLOCK-END a &#before b#& BLOCK-BEGIN b &##& BLOCK-END b &#',
            'before a#&BLOCK-BEGIN a &##& BLOCK-END a &##& BLOCK-BEGIN b &##& BLOCK-END b &#',
            'before a#&BLOCK-BEGIN a &##& BLOCK-END a &#and before b#& BLOCK-BEGIN b &##& BLOCK-END b &#',
        ]

        befores = [
            ['this comes before'], [''], [' '], [], ['', 'before b'], ['before a', ''], ['before a', 'and before b'],
        ]

        for idx in range(len(sources)):
            agr_file = AggregatingFile(sources[idx])
            block_befores = [b['before'] for b in agr_file.blocks]
            assert block_befores == befores[idx], 'Source:\n%s\nShould be %s but got %s' % (
                sources[idx], befores[idx], block_befores)

    def test_block_contents(self, *args, **kwargs):
        sources = [
            '#& BLOCK-BEGIN one &#this is some content#& BLOCK-END one &#',
            '#& BLOCK-BEGIN one &##& BLOCK-END one &#',
            '#&BLOCK-BEGIN one &# #& BLOCK-END one &#',
            'not a thing here',
            '#&BLOCK-BEGIN a &##& BLOCK-END a &##& BLOCK-BEGIN b &#content b#& BLOCK-END b &#',
            '#&BLOCK-BEGIN a &#content a#& BLOCK-END a &##& BLOCK-BEGIN b &##& BLOCK-END b &#',
            '#&BLOCK-BEGIN a &#content a#& BLOCK-END a &##& BLOCK-BEGIN b &#and content b#& BLOCK-END b &#',
        ]

        contents = [
            ['this is some content'], [''], [' '], [], ['', 'content b'], ['content a', ''],
            ['content a', 'and content b'],
        ]

        for idx in range(len(sources)):
            agr_file = AggregatingFile(sources[idx])
            block_contents = [b['contents'] for b in agr_file.blocks]
            assert block_contents == contents[idx], 'Source:\n%s\nShould be %s but got %s' % (
                sources[idx], contents[idx], block_contents)

    def test_inject(self, *args, **kwargs):
        sources = [
            ['hi#& BLOCK-BEGIN one &##& BLOCK-END one &#'],
            ['#& BLOCK-BEGIN one &#hi#& BLOCK-END one &#'],
            ['#& BLOCK-BEGIN one &##& BLOCK-END one &#hi'],
            ['hi#& BLOCK-BEGIN one &#hi#& BLOCK-END one &#'],
            ['#& BLOCK-BEGIN one &#hi#& BLOCK-END one &#hi'],
            ['hi#& BLOCK-BEGIN one &##& BLOCK-END one &#hi'],
            ['hi#& BLOCK-BEGIN one &#hi#& BLOCK-END one &#hi'],
            ['#& BLOCK-BEGIN a &#1#& BLOCK-END a &#', '#& BLOCK-BEGIN a &#2#& BLOCK-END a &#'],
            [
                '1#& BLOCK-BEGIN one &#2#& BLOCK-END one &#7',
                '#& BLOCK-BEGIN one &#34#& BLOCK-END one &#',
                '#& BLOCK-BEGIN one &#5#& BLOCK-END one &#',
                '#& BLOCK-BEGIN one &#6#& BLOCK-END one &#',
            ],
        ]

        blocks = [
            [],
            [],
            [],
            [],
            [],
            [],
            [],
            ['a'],
            ['one', 'one', 'one'],
        ]

        results = [
            'hi',
            'hi',
            'hi',
            'hihi',
            'hihi',
            'hihi',
            'hihihi',
            '12',
            '1234567',

        ]

        for idx in range(len(sources)):
            agr_file = []

            for inject_idx in range(0, len(sources[idx])):
                agr_file.append(AggregatingFile(sources[idx][inject_idx]))

                if inject_idx > 0:
                    file_contents = agr_file[inject_idx].get(blocks[idx][inject_idx - 1])['contents']
                    agr_file[0].inject(blocks[idx][inject_idx - 1], file_contents)

            assembled_file = agr_file[0].assemble()
            assert assembled_file == results[idx], \
                'Sources:\n%s\n should result in \'%s\', but instead got \'%s\'\n The blocks structure is:\n%s' \
                % (json.dumps(sources[idx], indent=4, sort_keys=True), results[idx], assembled_file,
                   json.dumps(agr_file[0].blocks, indent=4, sort_keys=True))

    def test_merge(self, *args, **kwargs):
        sources = [
            [
                '1#& BLOCK-BEGIN one &#2#& BLOCK-END one &#7',
                '#& BLOCK-BEGIN one &#34#& BLOCK-END one &#',
                '#& BLOCK-BEGIN one &#5#& BLOCK-END one &#',
                '#& BLOCK-BEGIN one &#6#& BLOCK-END one &#8',
            ],

            [
                '1 #& BLOCK-BEGIN 1 &#2 #& BLOCK-END 1 &#3 #& BLOCK-BEGIN 2 &#4 #& BLOCK-END 2 &#7 #& BLOCK-BEGIN 3 &##& BLOCK-END 3 &#9',
                '!! #& BLOCK-BEGIN 2 &#5 #& BLOCK-END 2 &#',
                '#& BLOCK-BEGIN 3 &#8 #& BLOCK-END 3 &##& BLOCK-BEGIN 2 &#6 #& BLOCK-END 2 &#!! #& BLOCK-BEGIN 4 &#!!!!#& BLOCK-END 4 &#',
                '#& BLOCK-BEGIN 4 &#!!!!#& BLOCK-END 4 &#',
                '!!!!#& BLOCK-BEGIN 5 &#!!!!#& BLOCK-END 5 &#!!',
            ],
            [
                '1\n#& BLOCK-BEGIN first &##& BLOCK-END first &#',
                '#& BLOCK-BEGIN first &#\n2\n\n#& BLOCK-END first &#',
                '#& BLOCK-BEGIN first &#\n3\n#& BLOCK-END first &#',
                '#& BLOCK-BEGIN first &#\n4\n#& BLOCK-END first &#',
            ],
        ]

        results = [
            '1234567',
            '1 2 3 4 5 6 7 8 9',
            '1\n2\n34',
        ]

        for idx in range(len(sources)):
            agg_files = []

            for src in sources[idx]:
                agg_files.append(AggregatingFile(src))

            for afile in agg_files[1:]:
                agg_files[0].merge(afile)

            assembled_file = agg_files[0].assemble()
            assert results[idx] == assembled_file, \
                'Sources:\n%s\n should result in \'%s\', but instead got \'%s\'\n The blocks structure is:\n%s' \
                % (json.dumps(sources[idx], indent=4, sort_keys=True), results[idx], assembled_file,
                   json.dumps(agg_files[0].blocks, indent=4, sort_keys=True))
