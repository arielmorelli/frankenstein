![alt text](docs/logo.png)

Frank makes it easier to start basic projects from a common skeleton

## How to use

1. Install it

  ```
  git clone https://github.com/rgcalsaverini/frankenstein
  cd frankenstein
  python setup.py install
  ```

2. Create a project

  ```
  $ frank myapp flask flask-sqlalchemy
  ```
3. Read the README.md on the section *next steps* to configure it
