from setuptools import setup

setup(name='frankenstein',
    version='0.1.0',
    description='Its aliiiiivee!',
    url='rui.calsaverini.com',
    author='Rui',
    author_email='rui@calsaverini.com',
    license='MIT',
    packages=['frankenstein'],
    install_requires=['docopt', 'flask', 'requests', 'unidecode'],
    scripts=['bin/frank'],
    zip_safe=False)
