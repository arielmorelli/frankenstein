import json

from flask import Flask

app = Flask(__name__)

_errors = {
    'invalid_uuid': {'success': False, 'message': 'Invalid project UUID'},
    'invalid_input': {'success': False, 'message': 'Invalid input data'},
    'failed_assemble': {'success': False, 'message': 'Failed to create project'},
}


def error(error_str, code=400):
    if error in _errors.keys():
        return json.dumps(_errors[error_str]), code
    return json.dumps({'success': False, 'message': error_str}), code
