import json
import os

from flask import request

from frankenstein import available_parts, assemble, config
from frankenstein.server import error


def view_parts():
    return json.dumps({'parts': available_parts})


def assemble_project():
    data = dict(request.args)

    if not data or 'parts' not in data.keys() or type(data['parts']) is not list or 'app_name' not in data.keys():
        return error('invalid_input')

    app_name = data['app_name'][0]
    parts = data['parts'][0].split(',')

    project_assembler = assemble.ProjectAssembler(app_name)
    status, message = project_assembler.add_parts(parts)
    if not status:
        return error(message)

    if not project_assembler.assemble():
        return error('failed_assemble')

    return json.dumps({'success': True, 'uuid': project_assembler.uuid})


def download_project(uuid):
    project_path = os.path.join(config['assembly_dir'], uuid)
    if not os.path.exists(project_path):
        return error('invalid_uuid')

    files = []

    for root, dirnames, filenames in os.walk(project_path):
        for filename in filenames:
            files.append({'path': os.path.join(root, filename).replace(project_path, '').strip('/')})

    for proj_file in files:
        with open(os.path.join(project_path, proj_file['path']), 'r') as f:
            proj_file['data'] = f.read()

    return json.dumps(files)
