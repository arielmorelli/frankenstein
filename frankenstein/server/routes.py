from frankenstein.server import views, app

routes = {
    '/projects': (views.assemble_project, ['GET']),
    '/projects/<string:uuid>': (views.download_project, ['GET']),
    '/parts': (views.view_parts, ['GET']),
}


def add_routes():
    for rule, endpoint_info in routes.iteritems():
        route_name = endpoint_info[0].__name__
        app.add_url_rule(rule, route_name, view_func=endpoint_info[0], methods=endpoint_info[1])
