import json
import os
from re import sub

import requests
import unidecode
from requests.exceptions import RequestException
from unidecode import unidecode

from frankenstein import config


class Project(object):
    url_base = '%s:%d' % (config['host'], config['port'])

    def __init__(self, app_name, parts):
        self.app_name = self.safe_name(app_name)
        self.parts = [part.lower() for part in parts]

        if app_name != self.app_name:
            raise ValueError('App name is invalid. May I suggest \'%s\'?' % self.app_name)

        if len(app_name) < 3:
            raise ValueError('App name is invalid. Pick onde with over 3 characters')

        if type(parts) is not list or len(parts) < 1:
            raise ValueError('Invalid part list provided')

    @staticmethod
    def safe_name(string):
        decoded = unidecode(unicode(string.decode('utf-8'))).lower()
        alpha_only = sub(r'[^a-z\s]+', '', decoded).strip()
        no_spaces = sub(r'\s+', '_', alpha_only)
        return no_spaces

    @staticmethod
    def _make_req(url):
        try:
            response = requests.get(url)
            response_json = json.loads(response.text)
        except RequestException:
            print 'Failed to reach frankenstein server'
            return False

        if response.status_code != 200:
            print 'Error: %s\n' % response_json['message']
            return False

        return response_json

    def get_project(self):
        uuid = self._assemble_request()

        if not uuid:
            return

        contents = self._get_project(uuid)
        self._create_project(contents)

    def _create_project(self, contents):
        pwd = os.getcwd()

        for project_file in contents:
            abs_file = os.path.join(os.getcwd(), self.app_name, project_file['path'])
            abs_dir = os.path.dirname(abs_file)

            if not os.path.exists(abs_dir):
                os.makedirs(abs_dir)

            with open(abs_file, 'w') as f:
                f.write(project_file['data'])

    def _assemble_request(self):
        url = '%s/projects?app_name=%s&parts=%s' % (self.url_base, self.app_name, ','.join(self.parts))
        result_body = self._make_req(url)

        if result_body:
            return result_body['uuid']
        return False

    def _get_project(self, uuid):
        url = '%s/projects/%s' % (self.url_base, uuid)

        result_body = self._make_req(url)
        if result_body:
            return result_body
        return False

    @staticmethod
    def show_parts():
        url = '%s/parts' % Project.url_base
        result_body = Project._make_req(url)
        if result_body:
            return result_body['parts']
        return []
