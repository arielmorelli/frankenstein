import re


class AggregatingFile(object):
    def __init__(self, file_contents):
        self.contents = file_contents
        self.blocks = []
        self.footer = ''

        self._separate_contents()

    def _separate_contents(self):
        block_names = self._get_block_names()
        content_left = self.contents

        for block in block_names:
            new_block = {'name': block, 'before': '', 'contents': ''}
            begin_regex = re.compile(r'#&\s*BLOCK-BEGIN\s+%s\s*&#' % block)
            end_regex = re.compile(r'#&\s*BLOCK-END\s+%s\s*&#' % block)
            content_parts = re.split(begin_regex, content_left)

            if len(content_parts) < 2:
                raise ValueError('Could not find block \'%s\'' % block)

            if len(content_parts) > 2:
                raise ValueError('Block \'%s\' already defined' % block)

            content_lower_parts = re.split(end_regex, content_parts[1])

            if len(content_lower_parts) < 2:
                raise ValueError('Block \'%s\' was not closed' % block)

            if len(content_lower_parts) > 2:
                raise ValueError('Block \'%s\' already closed' % block)

            new_block['before'] = self.header = content_parts[0]
            new_block['contents'] = content_lower_parts[0]
            content_left = content_lower_parts[1]
            self.blocks.append(new_block)

        if content_left.strip() != '':
            self.footer = content_left

    def _get_block_names(self):
        regex = re.compile(r'#&\s*BLOCK-BEGIN\s+([a-zA-Z0-9_-]+)\s*&#')
        return re.findall(regex, self.contents)

    # def __setattr__(self, key, value):
    #     self.inject(key, value)
    #
    # def __getattr__(self, item):
    #     return self.get(item)

    def inject(self, block_name, content, append=True):
        block = self.get(block_name)
        if append:
            block['contents'] += content
        else:
            block['contents'] = content

    def merge(self, agr_file, replace=False):
        for merge_into_block in self.blocks:
            try:
                merge_from_block = agr_file.get(merge_into_block['name'])
                contents = merge_from_block['contents']
                striped_contents = contents[1:] if len(contents) > 0 and contents[0] == '\n' else contents
                striped_contents = striped_contents[:-1] if len(contents) > 0 and striped_contents[-1] == '\n' else striped_contents
                if replace:
                    merge_into_block['contents'] = striped_contents
                else:
                    merge_into_block['contents'] += striped_contents
            except KeyError:
                pass

    def get(self, block_name):
        for block in self.blocks:
            if block['name'] == block_name:
                return block
        raise KeyError('No block called \'%s\' on defined this file' % block_name)

    def assemble(self):
        contents = ''
        for block in self.blocks:
            contents += block['before']
            contents += block['contents']

        contents += self.footer
        return contents
