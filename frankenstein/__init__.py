available_parts = ['flask', 'flask-sqlalchemy', 'flask-cache', 'daemon']

part_require = {
    'flask-sqlalchemy': ['flask'],
    'flask-cache': ['flask']
}

config = {
    'assembly_dir': '/tmp/frankenstein',
    'host': 'http://rui.calsaverini.com',
    'port': 2180,
}


def get_requirements_not_met(parts):
    requirements_not_met = {}
    for part in parts:
        if part in part_require.keys():
            for requirement in part_require[part]:
                if requirement not in parts:
                    requirements_not_met[part] = requirement
    return requirements_not_met
