import os
import random
from time import time

from . import available_parts, config, get_requirements_not_met
from .aggregating_file import AggregatingFile


def generate_uuid():
    now = int(time() * 1000000)
    uuid = str(hex(int('%d%d%d' % (random.randint(0, 999999), now, random.randint(0, 999999)))))
    return uuid[2:].upper()


class ProjectAssembler(object):
    def __init__(self, app_name):
        self.parts_dir = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../parts'))
        self.parts = ['base']
        self.uuid = generate_uuid()
        self.files = {}
        self.app_name = app_name

    def add_parts(self, part_list):
        if type(part_list) is not list:
            return False, 'Invalid input'

        not_met = get_requirements_not_met(part_list)
        if len(not_met) > 0:
            return False, 'Part \'%s\' depends on part \'%s\'' % (not_met.keys()[0], not_met[not_met.keys()[0]])

        for part in part_list:
            lower_part = part.lower()

            if lower_part not in available_parts:
                return False, 'Part \'%s\' is not a valid part. See frank --parts' % lower_part

        for part in available_parts:
            if part in part_list:
                self.parts.append(part)

        return True, ''

    def _get_part_files(self, part):
        part_path = os.path.join(self.parts_dir, part)

        part_files = []
        for root, dirnames, filenames in os.walk(part_path):
            for filename in filenames:
                part_files.append(os.path.join(root, filename).replace(part_path, '').strip('/'))

        return part_files

    def _get_all_parts_files(self):
        part_files = []

        for part in self.parts:
            part_files.append({'name': part, 'files': self._get_part_files(part)})
        return part_files

    @staticmethod
    def _get_file_contents(filename):
        with open(filename) as part_file:
            contents = part_file.read()
        return contents

    @staticmethod
    def _create_dir(path):
        if not os.path.exists(path):
            os.makedirs(path)

    def _create_project_dir(self):
        project_dir = os.path.abspath(os.path.join(config['assembly_dir'], self.uuid))
        self._create_dir(project_dir)
        return project_dir

    def assemble(self):
        project_dir = self._create_project_dir()

        for part_files_dict in self._get_all_parts_files():
            part_files = part_files_dict['files']
            part_name = part_files_dict['name']
            for part_filename in part_files:
                part_file_path = os.path.join(self.parts_dir, part_name, part_filename)
                contents = self._get_file_contents(part_file_path)
                contents = contents.replace('myprettyappname', self.app_name)
                contents = contents.replace('MyPrettyAppName', self.app_name.replace('_', ' ').title())
                part_file = AggregatingFile(contents)

                if part_filename in self.files.keys():
                    self.files[part_filename].merge(part_file)
                else:
                    self.files[part_filename] = part_file

            for project_filename, project_aggr_file in self.files.iteritems():
                file_path = os.path.join(project_dir, project_filename.replace('myprettyappname', self.app_name))
                self._create_dir(os.path.dirname(file_path))
                with open(file_path, "w") as project_file:
                    project_file.write(project_aggr_file.assemble())
        return True
