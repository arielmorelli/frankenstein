#& BLOCK-BEGIN imports &#
from flask import Flask
import json
#& BLOCK-END imports &#


#& BLOCK-BEGIN init &#
app = Flask(__name__)

_errors = {
    'invalid_person': {'success': False, 'message': 'Invalid person provided.'},
}


def error(error_str, code=400):
    if error in _errors.keys():
        return json.dumps(_errors[error_str]), code
    return json.dumps({'success': False, 'message': error_str}), code

#& BLOCK-END init &#

#& BLOCK-BEGIN after-app &##& BLOCK-END after-app &#
