from myprettyappname.server import views, app

routes = {
    '/json': (views.sample_json_view, ['GET']),
    '/person/<string:person>/<string:adjective>': (views.sample_template_view, ['GET']),
    '/person/<string:person>': (views.sample_template_view, ['GET']),
    '/person': (views.sample_template_view, ['GET']),
    '/': (views.sample_redirect, ['GET']),
#& BLOCK-BEGIN routes &##& BLOCK-END routes &#
}


def add_routes():
    for rule, endpoint_info in routes.iteritems():
        route_name = endpoint_info[0].__name__
        app.add_url_rule(rule, route_name, view_func=endpoint_info[0], methods=endpoint_info[1])
