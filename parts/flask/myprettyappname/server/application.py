from myprettyappname.server import routes, app #& BLOCK-BEGIN app-imports &##& BLOCK-END app-imports &#
#& BLOCK-BEGIN imports &##& BLOCK-END imports &#

#& BLOCK-BEGIN application &#
def run(port, debug, local):
    routes.add_routes()
    app.run(host='127.0.0.1' if local else '0.0.0.0', port=port, debug=debug)


def show_routes():
    max_length = max([len(k) for k in routes.routes.keys()]) + 5

    for rule in sorted(routes.routes.keys()):
        print '%s [%s]' % (rule.ljust(max_length), ','.join(routes.routes[rule][1]))
#& BLOCK-END application &#
