from myprettyappname.server import error#& BLOCK-BEGIN inline-imports &##& BLOCK-END inline-imports &#

import json
import random
from flask import render_template, redirect
#& BLOCK-BEGIN imports &##& BLOCK-END imports &#

_person = ['Ariel', 'Carlinhos', 'Dezao', 'Pumba', 'Sabrina', 'Ligia',
           'Felipe', 'Rui', 'Banheiro', 'Dino']

_adjective = ['is weird', 'is naughty', 'have a big head', 'is homeless', 'is chubby',
              'is very delicate', 'is kinda cute', 'is very modern', 'is special',
              'is barely functional', 'is graceful', 'is bashful', 'is rotten', 'should '
              'is juvenile', 'is troubled', 'is not very bright', 'have bad hair', 'can\'t sing'
              'feels pretty', 'is not real', 'loves grass', 'is high right now', 'wets the bed']


def get_person_and_adjective():
    person = random.choice(_person)
    adjective = random.choice(_adjective)
    return person, adjective


def sample_json_view():
    person, adjective = get_person_and_adjective()
    return json.dumps({"person": person, "adjective": adjective})


def sample_template_view(person=None, adjective=None):
    if person and person not in _person:
        return error('invalid_person')

    random_person, random_adjective = get_person_and_adjective()
    return render_template('/example.html',
                           person=person if person is not None else random_person,
                           adjective=adjective if adjective is not None  else random_adjective,
                           refresh_person=person is None,
                           refresh_adjective=adjective is None)


def sample_redirect():
    return redirect('/person')


#& BLOCK-BEGIN views &## #& BLOCK-END views &#
