#& BLOCK-BEGIN next-steps &#
### Flask

A basic demo flask app was deployed. There is little config to do here, just
replace it with your own application.

You should take a look at
```myprettyappname/server/routes.py``` and  ```myprettyappname/server/views.py```, those are probably the most important files.


#& BLOCK-END next-steps &#
