#& BLOCK-BEGIN docopt-usage &#
    myprettyappname.py server run [--port=<port_num>] [--debug] [--local]
    myprettyappname.py server routes
#& BLOCK-END docopt-usage &#

#& BLOCK-BEGIN docopt-options &#
    -p --port=<port_num>   Set server port [default: 5000]
    -D --debug             Run in debug mode
    -l --local             Run the server locally
#& BLOCK-END docopt-options &#


#& BLOCK-BEGIN imports &#
from myprettyappname.server import application

#& BLOCK-END imports &#

#& BLOCK-BEGIN main &#
if arguments['server']:
    if arguments['run']:
        application.run(port=int(arguments['--port']), debug=arguments['--debug'], local=arguments['--local'])

    if arguments['routes']:
        application.show_routes()

#& BLOCK-END main &#
