#& BLOCK-BEGIN next-steps &#
### Flask-SQLAlchemy

Before you do anything, go ahead and configure your DB settings on ```myprettyappname/config.py```, you should at least pick a password for the object ```db_user_pwd```

The **first time** running this app, execute ```python run.py db create --first_time``` to create the schema, tables and users. From that point on, use just ```python run.py db create``` to reset the database


#& BLOCK-END next-steps &#
