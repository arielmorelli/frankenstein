#& BLOCK-BEGIN docopt-usage &#

    myprettyappname.py db create [--first_time]
#& BLOCK-END docopt-usage &#

#& BLOCK-BEGIN docopt-options &#

    -f --first_time        Run this if it was never set up before
#& BLOCK-END docopt-options &#

#& BLOCK-BEGIN main &#

if arguments['db'] and arguments['create']:
    application.create(arguments['--first_time'])

#& BLOCK-END main &#
