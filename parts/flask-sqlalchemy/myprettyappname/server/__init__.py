#& BLOCK-BEGIN after-app &#
_db = [None]


def db():
    return _db[0]


def set_db(value):
    _db[0] = value


def ask_confirmation(message="Are you sure?"):
    confirmation = raw_input('%s%s' % (message, '[y/n]\n'))
    if confirmation.lower() in ['y', 'yes']:
        return True
    return False

#& BLOCK-END after-app &#
