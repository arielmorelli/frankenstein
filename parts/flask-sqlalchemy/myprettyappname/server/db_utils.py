import getpass

from flask_sqlalchemy import SQLAlchemy

from myprettyappname.server import db, set_db, app, ask_confirmation
from myprettyappname import config


def use_db():
    db().session.execute('USE %s' % config.db_name)
    db().engine.execute('USE %s' % config.db_name)


def create_db_user(username, password, location):
    credentials = (username, password)
    location = (location, username)
    db().session.execute('CREATE USER \'%s\'@\'localhost\' IDENTIFIED BY \'%s\'' % credentials)
    db().session.execute('GRANT ALL PRIVILEGES ON %s.* TO \'%s\'@\'%%\' WITH GRANT OPTION' % location)
    db().session.execute('FLUSH PRIVILEGES')


def init_db(override_db_settings=True):
    if override_db_settings:
        app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://%s:%s@localhost' % (config.db_user, config.db_user_pwd)
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    set_db(SQLAlchemy(app))


def first_time():
    print '* Provide the DB root credentials, it will be used for creating the app DB user:'
    user = raw_input("    db root username:  ")
    pwd = getpass.getpass("    db root password:  ")
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://%s:%s@localhost' % (user, pwd)
    init_db(False)
    create_db_user(config.db_user, config.db_user_pwd, config.db_name)


def create_models(drop=True):
    if drop:
        message = 'WARNING:\n This will COMPLETELY ERASE all data currently on the database.\n Do you want to proceed?'
        if not ask_confirmation(message):
            exit()
        db().session.execute('DROP DATABASE %s' % config.db_name)
    db().session.execute('CREATE DATABASE %s' % config.db_name)
    use_db()
    import models
    models.create_all()
