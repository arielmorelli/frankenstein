from myprettyappname.server import db_utils, app

#& BLOCK-BEGIN app-imports &#
,db_utils
#& BLOCK-END app-imports &#

#& BLOCK-BEGIN application &#
def create(first_time):
    if first_time:
        db_utils.first_time()
    db_utils.init_db()
    db_utils.create_models(not first_time)
    if first_time:
        exit()
#& BLOCK-END application &#
