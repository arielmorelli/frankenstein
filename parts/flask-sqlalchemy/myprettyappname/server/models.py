from myprettyappname.server import db as app_db

db = app_db()


class SampleModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)


def create_all():
    db.create_all()
