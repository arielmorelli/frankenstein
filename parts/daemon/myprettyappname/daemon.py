import sys
import os
import time
import atexit
from signal import SIGTERM


class Daemon:
    """
    Usage: subclass the Daemon class and override the run() method
    """

    def __init__(self, pid_file, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.pid_file = pid_file

    def daemonize(self):
        try:
            pid = os.fork()
            if pid > 0:
                # exit first parent
                sys.exit(0)
        except OSError, e:
            sys.stderr.write("fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)

        # decouple from parent environment
        os.chdir("/")
        os.setsid()
        os.umask(0)

        try:
            pid = os.fork()
            if pid > 0:
                # exit from second parent
                sys.exit(0)
        except OSError, e:
            sys.stderr.write("fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)

        # redirect standard file descriptors
        sys.stdout.flush()
        sys.stderr.flush()
        si = file(self.stdin, 'r')
        so = file(self.stdout, 'a+')
        se = file(self.stderr, 'a+', 0)
        os.dup2(si.fileno(), sys.stdin.fileno())
        os.dup2(so.fileno(), sys.stdout.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())

        atexit.register(self._delete_pid_file)
        pid = str(os.getpid())
        file(self.pid_file, 'w+').write("%s\n" % pid)

    def _delete_pid_file(self):
        os.remove(self.pid_file)

    def start(self):
        try:
            pf = file(self.pid_file, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None

        if pid:
            message = "Cannot start, already running.\n"
            sys.stderr.write(message)
            sys.exit(1)

        # Start the daemon
        self.daemonize()
        self.run()

    def stop(self):
        pid = self._get_pid_from_file()

        if not pid:
            message = "Cannot stop, not running.\n"
            sys.stderr.write(message)
            return  # not an error in a restart

        # Try killing the daemon process
        try:
            while 1:
                os.kill(pid, SIGTERM)
                time.sleep(0.1)
        except OSError, err:
            err = str(err)
            if err.find("No such process") > 0:
                if os.path.exists(self.pid_file):
                    os.remove(self.pid_file)
            else:
                print str(err)
                sys.exit(1)

    def _get_pid_from_file(self):
        # Get the pid from the pid_file
        try:
            pf = file(self.pid_file, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None

        return pid

    def restart(self):
        self.stop()
        self.start()

    def status(self):
        pid = self._get_pid_from_file()

        if not pid:
            message = "Not running\n"
            sys.stderr.write(message)

        else:
            message = self._running_status()
            sys.stderr.write(message)

    def _running_status(self):
        raise NotImplementedError(
                'You should subclass the Daemon class and then override the method _running_status. '
                'It should return a string with the status report of the daemon if it is running')

    def run(self):
        raise NotImplementedError(
                'You should subclass the Daemon class and then override the method run. '
                'It will be called after the process has been daemonized by start() or restart().')
