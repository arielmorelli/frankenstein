#& BLOCK-BEGIN docopt-usage &#

    myprettyappname.py service (start|restart|stop|status)
#& BLOCK-END docopt-usage &#

#& BLOCK-BEGIN imports &#
from myprettyappname.daemon import Daemon
import time
#& BLOCK-END imports &#

#& BLOCK-BEGIN main &#
class MyPrettyAppNameDaemon(Daemon):

    def run(self):
        while True:
            time.sleep(1)

    def _running_status(self):
        return 'Running.'

if arguments['service']:
    daemon = MyPrettyAppNameDaemon('/tmp/myprettyappname-#& password(5) &#.pid')

    if arguments['start']:
        daemon.start()
    if arguments['restart']:
        daemon.restart()
    if arguments['stop']:
        daemon.stop()
    if arguments['status']:
        daemon.status()

#& BLOCK-END main &#
