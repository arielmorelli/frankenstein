#& BLOCK-BEGIN imports &#
from flask.ext.cache import Cache
#& BLOCK-END imports &#


#& BLOCK-BEGIN after-app &#
cache = Cache(app, config={'CACHE_TYPE': 'simple'})

#& BLOCK-END after-app &#
