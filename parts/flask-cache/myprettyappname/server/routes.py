#& BLOCK-BEGIN routes &#
    '/cache/simple': (views.cache_simple, ['GET']),
    '/cache/memoized/<a>/<b>': (views.cache_memoized, ['GET']),
    '/cache/invalidate': (views.cache_invalidate, ['GET']),
#& BLOCK-END routes &#
