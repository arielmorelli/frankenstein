#& BLOCK-BEGIN inline-imports &#
, cache
#& BLOCK-END inline-imports &#

#& BLOCK-BEGIN imports &#
import time
#& BLOCK-END imports &#

#& BLOCK-BEGIN views &#
@cache.cached(timeout=60)
def cache_simple():
    return "Simple cache keeps the rendered page for a preset amount of time, after which it renders it again" \
        "<br>\nGenerated at %s" % time.strftime("%H:%M:%S")

@cache.memoize(60)
def cache_memoized(a, b):
    return "Memoized cache remembers the view parameters.\n<br> PAR A: %s\n<br>PAR B: %s" \
        "<br>\nGenerated at %s" % (a, b, time.strftime("%H:%M:%S"))

def cache_invalidate():
    cache.delete_memoized('cache_memoized')
    cache.clear()
    return 'Memoized cache can be explicitly invalidated for a single view, or for the whole application'

# #& BLOCK-END views &#