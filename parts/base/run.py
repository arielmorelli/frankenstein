"""myprettyappname - myprettyappname

Usage:
#& BLOCK-BEGIN docopt-usage &##& BLOCK-END docopt-usage &#
    myprettyappname.py --help

Options:
#& BLOCK-BEGIN docopt-options &##& BLOCK-END docopt-options &#
    -h --help              Show this screen.
"""

from docopt import docopt
#& BLOCK-BEGIN imports &##& BLOCK-END imports &#

arguments = docopt(__doc__)
#& BLOCK-BEGIN main &##& BLOCK-END main &#
