# MyPrettyAppName #

Description here

--------------------------------

## Next Steps (Read this first!) ##
This is here because you used **frank** to generate this project.

After you complete the following steps remove this part of the README. All the other parts could be left here.

#& BLOCK-BEGIN next-steps &##& BLOCK-END next-steps &#

--------------------------------


## How to install ##
### Development ###
Create a virtualenv and install the requirements
```
virtualenv env
pip install -r requirements.txt --upgrade
```
### Production ###
Execute the setup

```
python setup.py install
```
